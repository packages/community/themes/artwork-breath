# Maintainer: Mark Wagie <mark@manjaro.org>
# Maintainer: Artem Grinev <agrinev@manjaro.org>
# Contributor: Bernhard Landauer <bernhard@manjaro.org>

pkgbase=artwork-breath
pkgname=(
    'plasma6-themes-breath-migration'
    'plasma6-themes-breath'
    'plasma6-themes-breath-extra'
    'breath-wallpapers'
    'sddm-breath-theme'
)
pkgver=24.0.0
pkgrel=4
arch=('any')
url="https://gitlab.manjaro.org/artwork/themes/breath"
license=('LGPL-2.0-or-later AND CC-BY-SA-4.0 AND GPL-3.0-only AND GPL-2.0-or-later')
makedepends=('cmake' 'extra-cmake-modules' 'git' 'libplasma')
_commit=0266802cdbe62a9f18c9fdb9630732d60e49fa90  # 24.0.0
source=("git+$url.git#commit=${_commit}")
sha256sums=('da137cb462b1cbc7a1aae8fc46f5010288b2cb87da9324d58252da3cb898fabc')

build() {

  echo "Building plasma6-themes-breath-migration..."
  cmake -B build-breath-migration -S breath \
    -DCMAKE_BUILD_TYPE='Release' \
    -DCMAKE_INSTALL_PREFIX='/usr' \
    -DKDE_INSTALL_USE_QT_SYS_PATHS='ON' \
    -DBUILD_MIGRATION='ON'
  cmake --build build-breath-migration

  echo "Building plasma6-themes-breath..."
  cmake -B build-breath -S breath \
    -DCMAKE_BUILD_TYPE='Release' \
    -DCMAKE_INSTALL_PREFIX='/usr' \
    -DKDE_INSTALL_USE_QT_SYS_PATHS='ON' \
    -DBUILD_PLASMA_THEMES='ON'
  cmake --build build-breath

  echo "Building plasma6-themes-breath-extra..."
  cmake -B build-breath-extra -S breath \
    -DCMAKE_BUILD_TYPE='Release' \
    -DCMAKE_INSTALL_PREFIX='/usr' \
    -DKDE_INSTALL_USE_QT_SYS_PATHS='ON' \
    -DBUILD_EXTRA_COLORS='ON'
  cmake --build build-breath-extra

  echo "Building sddm-breath-theme..."
  cmake -B build-sddm -S breath \
    -DCMAKE_BUILD_TYPE='Release' \
    -DCMAKE_INSTALL_PREFIX='/usr' \
    -DKDE_INSTALL_USE_QT_SYS_PATHS='ON' \
    -DBUILD_SDDM_THEME='ON'
  cmake --build build-sddm
}

package_plasma6-themes-breath-migration() {
  pkgdesc="Breath theme migration routine for kconf_update"
  arch=('x86_64' 'aarch64')
  conflicts=('plasma5-themes-breath-migration')
  replaces=('plasma5-themes-breath-migration')

  DESTDIR="$pkgdir" cmake --install build-breath-migration
}

package_plasma6-themes-breath() {
  pkgdesc="Breath theme for Plasma 6"
  depends=('breeze' 'plasma6-themes-breath-migration')
  conflicts=('plasma5-themes-breath' 'plasma5-themes-breath2' 'breath2-icon-themes')
  replaces=('plasma5-themes-breath' 'plasma5-themes-breath2' 'breath2-icon-themes')

  DESTDIR="$pkgdir" cmake --install build-breath
}

package_plasma6-themes-breath-extra() {
  pkgdesc="Additional Breath colors for Plasma 6"

  DESTDIR="$pkgdir" cmake --install build-breath-extra
}

package_breath-wallpapers() {
  pkgdesc="Breath wallpapers"

  cd breath/wallpapers
  install -Dm644 Bamboo/contents/images/5120x2880.png \
    "$pkgdir"/usr/share/backgrounds/bamboo.png
  install -Dm644 Bamboo\ at\ Night/contents/images/5120x2880.png \
    "$pkgdir"/usr/share/backgrounds/bambooatnight.png

  install -Dm644 North/contents/images/5120x2880.png \
    "$pkgdir"/usr/share/backgrounds/north.png
  install -Dm644 North/contents/images_dark/5120x2880.png \
    "$pkgdir"/usr/share/backgrounds/northdark.png
}

package_sddm-breath-theme() {
  pkgdesc="Breath theme for SDDM"
  depends=('libplasma' 'plasma-workspace' 'plasma6-themes-breath')
  conflicts=('sddm-breath2-theme')
  replaces=('sddm-breath2-theme')

  DESTDIR="$pkgdir" cmake --install build-sddm
}
